//
//  SettingsTableViewCell.swift
//  SomethingLikePomodoro
//
//  Created by Эльвира on 19.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var settingSwitch: UISwitch!
    @IBOutlet weak var tittleLabel: UILabel!
    var indexPth : NSIndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func switchValueChanged(sender: UISwitch) {
        
        
        if (self.indexPth.row == 2) {
            self.settings.ticTakSound = sender.on
            NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreAndWait()
        } else if (self.indexPth.row == 3) {
            self.settings.alarm = sender.on
            NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreAndWait()
        }
    }
    
    var settings : Settings = {
        return Settings.MR_findAll()[0] as Settings
        }()
    
    
    
    }
