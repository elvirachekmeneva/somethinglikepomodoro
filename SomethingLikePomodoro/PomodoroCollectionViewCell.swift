//
//  PomodoroCollectionViewCell.swift
//  
//
//  Created by Эльвира on 19.01.15.
//
//

import UIKit

class PomodoroCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var image: UIImageView!
    @IBOutlet var lengthLbl: UILabel!
}
