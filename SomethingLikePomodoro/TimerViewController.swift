//
//  TimerViewController.swift
//  SomethingLikePomodoro
//
//  Created by Эльвира on 12.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController, UIAlertViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, NSFetchedResultsControllerDelegate {

    @IBOutlet var timerLabel: UILabel!
    
    @IBOutlet var stopBtn: UIButton!
    @IBOutlet var pauseBtn: UIButton!
    @IBOutlet var doneBtn: UIButton!
    
    @IBOutlet var settingsBtn: UIButton!
    
    @IBOutlet var todayPomodorosCountLbl: UILabel!
    @IBOutlet var planCountLbl: UILabel!
    
    @IBOutlet weak var pomodorosCollectionView: UICollectionView!
    var stopAlert : UIAlertView!
    var dayPlan : Int?
    
    
    var todayPomodoros : NSMutableArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pomodorosCollectionView.layer.cornerRadius = 10
        todayPomodoros = []
        MagicalRecord.setupCoreDataStack()
        self.pomodoroFetchController.delegate = self
        self.settingsFetchController.delegate = self
        Timer.sharedInstance.timeLabel = self.timerLabel
        self.timerLabel.text = "Start"
        
        var set : Settings!
        if (self.settingsFetchController.fetchedObjects?.count > 0){
            set = self.settingsFetchController.fetchedObjects![0] as Settings
            
            self.todayPomodorosCountLbl.text = "\(self.pomodoroFetchController.fetchedObjects!.count)"
            self.planCountLbl.text = "\(set.dayPlan!.integerValue)"
            self.dayPlan = set.dayPlan?.integerValue
        } else {
            Settings.create({ (settings) -> Void in
                settings.alarm = true
                settings.ticTakSound = true
                settings.dayPlan = 10
                settings.pomodoroLength = 1500
                
                }, completion: { (success, error) -> Void in
                    set = self.settingsFetchController.fetchedObjects![0] as Settings
                    
                    self.todayPomodorosCountLbl.text = "\(self.pomodoroFetchController.fetchedObjects!.count)"
                    self.planCountLbl.text = "\(set.dayPlan!.integerValue)"
                    self.dayPlan = set.dayPlan?.integerValue
            })
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBar.hidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        navigationController?.navigationBar.hidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - UI actions
    @IBAction func stopBtnTapped(sender: AnyObject) {
        if Timer.sharedInstance.timerOn == true {
            if self.stopAlert == nil {
                self.stopAlert = UIAlertView(title: "Stop pomodoro", message: "This pomodoro will be deleted. Continue?", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Cancel", "Continue")
            }
            
            self.stopAlert.show()
        }
    }
    
    @IBAction func startTimer(sender: AnyObject) {
        if Timer.sharedInstance.timerOn == false {
            Timer.sharedInstance.start()
        }
    }
    
    
    
    //MARK: - UIAlertViewDelegate methods
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.isEqual(self.stopAlert){
            switch buttonIndex{
                
            case 0:
                NSLog("Cancel");
                break;
            case 1:
                NSLog("Continue");
                Timer.sharedInstance.stop()
                break;
            default:
                NSLog("Default");
                break;
                
            }
        }
        
    }
    
    //MARK: - UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        var set : Settings!
        var plan = 0
        if self.settingsFetchController.fetchedObjects?.count > 0 {
            set = self.settingsFetchController.fetchedObjects![0] as Settings
            plan = Int(set.dayPlan!)
        }
        
        var count = 0
        if (self.pomodoroFetchController.sections?.count > 0) {
            let sectionInfo: NSFetchedResultsSectionInfo = self.pomodoroFetchController.sections?[section] as NSFetchedResultsSectionInfo
            if (sectionInfo.numberOfObjects > 0 ){
                count = sectionInfo.numberOfObjects
            }
        }
        
        if (count <= plan) {
            return plan
        } else {
            return count
        }
        
       
    }
    
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        var cell : PomodoroCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("pomodoro", forIndexPath: indexPath) as PomodoroCollectionViewCell
        
        if (self.pomodoroFetchController.fetchedObjects?.count > 0) {
            
            var pomodoro : Pomodoro!
            
            if (self.pomodoroFetchController.fetchedObjects?.count > indexPath.item ){
                cell.image.image = UIImage(named: "greenCircle.png")
                pomodoro = self.pomodoroFetchController.fetchedObjects![indexPath.item] as? Pomodoro
                cell.lengthLbl.text = "\(Int(Int(pomodoro.length!) / 60))"
            } else {
                cell.image.image = UIImage(named: "grayCircle.png")
                cell.lengthLbl.text = ""
            }
            
            if (indexPath.item >= self.dayPlan) {
                cell.image.image = UIImage(named: "redCircle.png")
                pomodoro = self.pomodoroFetchController.fetchedObjects![indexPath.item] as? Pomodoro
                cell.lengthLbl.text = "\(Int(Int(pomodoro.length!) / 60))"
            }
        } else {
            cell.image.image = UIImage(named: "grayCircle.png")
            cell.lengthLbl.text = ""
            
        }
        
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        return 1
    }
    
    
    //MARK: -
    //MARK: other methods
    
    var pomodoroFetchController:NSFetchedResultsController = {
        let calendar = NSCalendar.currentCalendar()
        let flags: NSCalendarUnit = .DayCalendarUnit | .MonthCalendarUnit | .YearCalendarUnit | .HourCalendarUnit | .MinuteCalendarUnit | .SecondCalendarUnit
        let components = calendar.components(flags, fromDate: NSDate())
        components.hour = 0
        components.minute = 0
        components.second = 0
        let startDate = calendar.dateFromComponents(components)
        components.hour = 23
        components.minute = 59
        components.second = 59
        let endDate = calendar.dateFromComponents(components)
       
        MagicalRecord.setupCoreDataStack()
        
        return  Pomodoro.MR_fetchAllGroupedBy(nil, withPredicate: NSPredicate(format: "(date >= %@) AND (date <= %@)", startDate!, endDate!), sortedBy: "date", ascending: true)
    }()
    
    var settingsFetchController:NSFetchedResultsController = {
        MagicalRecord.setupCoreDataStack()
        return  Settings.MR_fetchAllGroupedBy(nil, withPredicate: NSPredicate(format: "dayPlan > 0"), sortedBy: "dayPlan", ascending: true)
        }()
    
    
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        if (controller.isEqual(self.pomodoroFetchController)) {
            self.todayPomodorosCountLbl.text = "\(self.pomodoroFetchController.fetchedObjects!.count)"
            self.pomodorosCollectionView.reloadData()
        }
        
        if (controller.isEqual(self.settingsFetchController)) {
            var set : Settings = self.settingsFetchController.fetchedObjects![0] as Settings
            self.todayPomodorosCountLbl.text = "\(self.pomodoroFetchController.fetchedObjects!.count)"
            self.planCountLbl.text = "\(set.dayPlan!.integerValue)"
            self.dayPlan = set.dayPlan?.integerValue


            self.pomodorosCollectionView.reloadData()
            
        }
    }

    
}
