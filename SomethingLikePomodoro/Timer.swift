//
//  Timer.swift
//  SomethingLikePomodoro
//
//  Created by Эльвира on 12.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

import UIKit
import AVFoundation

class Timer: NSObject, NSFetchedResultsControllerDelegate {
    
    private var timer : NSTimer!
    var startTime : NSDate?
    var endTime : NSDate?
    weak var timeLabel : UILabel?
    var duration : NSTimeInterval!
    var savePomodoro : Bool!
    var pomodoroLength : Int?
    var timerOn = false
    var soundOn = false
    var alarmOn = false
    
    var backgroundMusic = AVAudioPlayer()
    
    class var sharedInstance : Timer {
        struct Static {
            static let instance : Timer = Timer()
        }
         Static.instance.settingsFetchController.delegate =  Static.instance
        
        return Static.instance
    }
    
    var length : Int {
        get {
            return self.length
        }
    }
    
    func start() {
        self.timerOn = true
//        var set : Settings = Settings.MR_findAll()[0] as Settings
        var setts : Settings = self.settingsFetchController.fetchedObjects![0] as Settings
        
        
        self.pomodoroLength = setts.pomodoroLength?.integerValue
        duration = setts.pomodoroLength?.doubleValue
        self.startTime = NSDate()
        self.endTime = self.startTime!.dateByAddingTimeInterval(duration)
        
//        var setts : Settings = self.settingsFetchController.fetchedObjects![0] as Settings
        self.soundOn = setts.ticTakSound!.boolValue
        self.alarmOn = setts.alarm!.boolValue
        self.backgroundMusic = self.setupAudioPlayerWithFile("ticTak", type:"mp3")
        self.playSound()
        
        self.savePomodoro = true
        self.updateTimerLabel()
        
        
    }
    
    func stop() {
        self.savePomodoro = false
        endTime = NSDate()
        self.updateTimerLabel()
    }
    
    
    func updateTimerLabel() {
        duration = endTime!.timeIntervalSinceDate(NSDate())
        if duration < 0 {
            self.timeLabel?.text = "Start"
            self.timerOn = false
            if self.savePomodoro == true {
                Pomodoro.create({ (pomodoro) -> Void in
                    pomodoro.date = NSDate()
                    pomodoro.length = self.pomodoroLength
                    
                    }, completion: { (success, error) -> Void in
                        
                })
                if self.alarmOn == true {
                    var localNotification:UILocalNotification = UILocalNotification()
                    localNotification.alertAction = "Pomodoro completed!"
                    localNotification.alertBody = "You can take a little break :)"
                    localNotification.fireDate = NSDate()
                    localNotification.soundName = UILocalNotificationDefaultSoundName
                    UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                }
            }
            
            if self.backgroundMusic.playing {
                self.backgroundMusic.stop()
            }
            return
        }
        let minutes = UInt32(duration / 60.0)
        let seconds = UInt32(duration) % 60
        self.timeLabel?.text = NSString(format: "%02i:%02i", minutes,seconds)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
                NSThread.sleepForTimeInterval(1)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.updateTimerLabel()
                })
            })
            
        
    }
    
    func playSound() {
        if self.timerOn {
            if (self.soundOn) {
                self.backgroundMusic.volume = 0.3
                self.backgroundMusic.play()
            } else {
                if self.backgroundMusic.playing {
                    self.backgroundMusic.stop()
                }
            }
        }
    }
    
    var settingsFetchController:NSFetchedResultsController = {
        return  Settings.MR_fetchAllGroupedBy(nil, withPredicate: NSPredicate(format: "dayPlan > 0"), sortedBy: "dayPlan", ascending: true)
        }()
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        
        if (controller.isEqual(self.settingsFetchController)) {
            var set : Settings = self.settingsFetchController.fetchedObjects![0] as Settings
            self.soundOn = set.ticTakSound!.boolValue
            self.alarmOn = set.alarm!.boolValue
            self.playSound()
        }
    }
    
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer  {
        //1
        var path = NSBundle.mainBundle().pathForResource(file, ofType:type)
        var url = NSURL.fileURLWithPath(path!)
        
        //2
        var error: NSError?
        
        //3
        var audioPlayer:AVAudioPlayer?
        audioPlayer = AVAudioPlayer(contentsOfURL: url, error: &error)
        
        //4
        return audioPlayer!
    }

   
}
