// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to App.swift instead.

import CoreData

enum AppAttributes: String {
    case firstStart = "firstStart"
}

@objc
class _App: NSManagedObject {

    // MARK: - Class methods

    class func entityName () -> String {
        return "App"
    }

    class func entity(managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entityForName(self.entityName(), inManagedObjectContext: managedObjectContext);
    }

    // MARK: - Life cycle methods

    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }

    convenience init(managedObjectContext: NSManagedObjectContext!) {
        let entity = _App.entity(managedObjectContext)
        self.init(entity: entity, insertIntoManagedObjectContext: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged
    var firstStart: NSNumber?

    // func validateFirstStart(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    // MARK: - Relationships

}

