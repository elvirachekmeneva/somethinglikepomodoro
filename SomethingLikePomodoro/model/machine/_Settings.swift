// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Settings.swift instead.

import CoreData

enum SettingsAttributes: String {
    case alarm = "alarm"
    case dayPlan = "dayPlan"
    case pomodoroLength = "pomodoroLength"
    case ticTakSound = "ticTakSound"
}

@objc
class _Settings: NSManagedObject {

    // MARK: - Class methods

    class func entityName () -> String {
        return "Settings"
    }

    class func entity(managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entityForName(self.entityName(), inManagedObjectContext: managedObjectContext);
    }

    // MARK: - Life cycle methods

    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }

    convenience init(managedObjectContext: NSManagedObjectContext!) {
        let entity = _Settings.entity(managedObjectContext)
        self.init(entity: entity, insertIntoManagedObjectContext: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged
    var alarm: NSNumber?

    // func validateAlarm(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var dayPlan: NSNumber?

    // func validateDayPlan(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var pomodoroLength: NSNumber?

    // func validatePomodoroLength(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var ticTakSound: NSNumber?

    // func validateTicTakSound(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    // MARK: - Relationships

}

