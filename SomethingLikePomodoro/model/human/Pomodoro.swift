@objc(Pomodoro)
class Pomodoro: _Pomodoro {

    class func create(creation:(pomodoro:Pomodoro)->Void, completion:(success:Bool?,error:NSError?)->Void) {
        MagicalRecord.saveWithBlock({ (localContext) -> Void in
            var pomodoro = Pomodoro.MR_createInContext(localContext) as Pomodoro
            
            creation(pomodoro: pomodoro)
            }, completion: { (success, error) -> Void in
                completion(success: success, error: error)
        })
    }


}
