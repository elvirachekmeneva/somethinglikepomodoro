@objc(Settings)
class Settings: _Settings {

	// Custom logic goes here.

    class func create(creation:(settings:Settings)->Void, completion:(success:Bool?,error:NSError?)->Void) {
        MagicalRecord.saveWithBlock({ (localContext) -> Void in
            var settings = Settings.MR_createInContext(localContext) as Settings
            
            creation(settings: settings)
            }, completion: { (success, error) -> Void in
                completion(success: success, error: error)
        })
    }

    
}
