@objc(App)
class App: _App {

	// Custom logic goes here.
    class func create(creation:(app:App)->Void, completion:(success:Bool?,error:NSError?)->Void) {
        MagicalRecord.saveWithBlock({ (localContext) -> Void in
            var app = App.MR_createInContext(localContext) as App
            
            creation(app: app)
            }, completion: { (success, error) -> Void in
                completion(success: success, error: error)
        })
    }
}
