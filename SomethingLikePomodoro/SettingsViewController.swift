//
//  SettingsViewController.swift
//  SomethingLikePomodoro
//
//  Created by Эльвира on 12.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

import UIKit

struct PomodoroDuration {
    var durationTitle : String
    var durationLength : Int
}

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet var pickerBottomSpace: NSLayoutConstraint!
    @IBOutlet var planPicker: UIPickerView!
    @IBOutlet weak var settingsTableView: UITableView!
    var currentDuration : PomodoroDuration?
    var durations : [PomodoroDuration]! = [PomodoroDuration(durationTitle: "25 minutes", durationLength: 1500),
                        PomodoroDuration(durationTitle: "20 minutes", durationLength: 1200),
                        PomodoroDuration(durationTitle: "15 minutes", durationLength: 900),
                        PomodoroDuration(durationTitle: "10 minutes", durationLength: 600),
                        PomodoroDuration(durationTitle: "5 minutes", durationLength: 300),
                        PomodoroDuration(durationTitle: "1 minute", durationLength: 60) ]
    
    var settingsTitles = ["Pomodoro length", "Day plan", "Sound (tick-tack)", "Alarm"]
    
    
    override func viewDidLoad() {
        self.currentDuration = PomodoroDuration(durationTitle: "\(Int(Int(self.settings.pomodoroLength!)/60)) minutes", durationLength: Int(self.settings.pomodoroLength!))
        
        self.settingsFetchController.delegate = self
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var settings : Settings = {
        return Settings.MR_findAll()[0] as Settings
    }()
    
    var settingsFetchController:NSFetchedResultsController = {
        return  Settings.MR_fetchAllGroupedBy(nil, withPredicate: NSPredicate(format: "dayPlan > 0"), sortedBy: "dayPlan", ascending: true)
        }()
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.settingsTableView.reloadData()
    }
    

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : SettingsTableViewCell!
        var settings : Settings = self.settingsFetchController.fetchedObjects![0] as Settings
        

        if indexPath.row == 0 || indexPath.row == 1 {
            cell = tableView.dequeueReusableCellWithIdentifier("infoCell") as SettingsTableViewCell
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("switchCell") as SettingsTableViewCell
        }
        cell.indexPth = indexPath
        cell.tittleLabel.text = self.settingsTitles[indexPath.row]
        switch indexPath.row {
        case 0 :
            cell.infoLabel.text = self.currentDuration?.durationTitle
        case 1:
            cell.infoLabel.text = "\(settings.dayPlan!)"
        case 2:
            cell.settingSwitch.on = settings.ticTakSound!.boolValue
        case 3:
            cell.settingSwitch.on = settings.alarm!.boolValue
        default: break
        }
        return cell
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var sett : Settings = self.settingsFetchController.fetchedObjects![0] as Settings
        
        if (indexPath.row == 0) {
            let durationsMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
            for pomDuration in self.durations {
                let button = UIAlertAction(title: pomDuration.durationTitle, style: UIAlertActionStyle.Default) { (alert) -> Void in
                    self.currentDuration = pomDuration
                    self.settings.pomodoroLength = pomDuration.durationLength
                    NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreAndWait()
                    self.settingsTableView.reloadData()
                }
                durationsMenu.addAction(button)
            }
            
            self.presentViewController(durationsMenu, animated: true, completion: nil)
        }
        if (indexPath.row == 1) {
            self.settingsTableView.userInteractionEnabled = false
            UIView.animateWithDuration( 0.25, animations: { () -> Void in
                    self.pickerBottomSpace.constant = 0
                    self.planPicker.selectRow(Int(sett.dayPlan!) - 1, inComponent: 0, animated: false)
                    self.settingsTableView.alpha = 0.5
                
                
            })
            
            
        }
    }
    
    //MARK: - UIPickerView
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return 101
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return "\(row+1)"
    }

    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.settings.dayPlan = row + 1
        NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreAndWait()

        self.settingsTableView.userInteractionEnabled = true
        UIView.animateWithDuration( 0.25, animations: { () -> Void in
            self.pickerBottomSpace.constant = -162
            self.settingsTableView.alpha = 1
            self.settingsTableView.reloadData()
            
        })
    }
    
    
}
